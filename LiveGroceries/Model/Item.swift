//
//  Item.swift
//  LiveGroceries
//
//  Created by Roie Shimon Cohen on 16/10/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import Foundation

struct Item: Decodable {
    var name: String
    var weight: String
    var bagColor: String
    
    var weightValue: Float {
        let valueStr = weight.replacingOccurrences(of: "kg", with: "") as NSString
        let value = valueStr.floatValue
        if value > 0 {
             return value
        }
        return 0
    }
}
