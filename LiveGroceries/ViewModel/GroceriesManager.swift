//
//  GroceriesManager.swift
//  LiveGroceries
//
//  Created by Roie Shimon Cohen on 16/10/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import Foundation

protocol GroceriesFeed: AnyObject {
    func itemAdded(_ item: Item, index: Int)
}

class GroceriesManager {

    static let shared = GroceriesManager()
    weak var delegate: GroceriesFeed?
    
    let wsUrlString = "ws://superdo-groceries.herokuapp.com/receive"
    var webSocketTask: URLSessionWebSocketTask?
    
    var items = [Item]()
    var minimumWeight: Float?
    
    
    //MARK: - WebSocket
    func connectToWebSocket() {
        let urlSession = URLSession(configuration: .default)
        guard let url = URL(string: wsUrlString) else { return }
        webSocketTask = urlSession.webSocketTask(with: url)
        webSocketTask?.resume()
        listen()
    }
    
    func listen() {
        webSocketTask?.receive { result in
            switch result {
            case .failure(let error):
                print("Error in receiving message: \(error)")
            case .success(let message):
                switch message {
                case .string(let str):
                    print("Received string: \(str)")
                    guard let data = str.data(using: .utf8) else { return }
                    self.parseData(data)
                case .data(let data):
                    print("Received data: \(data)")
                    self.parseData(data)
                @unknown default:
                    break
                }
                
                self.listen()
            }
        }
    }
    
    
    //MARK: - Handle data
    func parseData(_ data: Data) {
        let decoder = JSONDecoder()
        do {
            let item = try decoder.decode(Item.self, from: data)
            filterItem(item: item)
            print("item: \(item) weight: \(item.weightValue)")
        } catch  {
            print("Error parsing")
        }
    }
    
    func filterItem(item: Item) {
        func addItem() {
            items.append(item)
            DispatchQueue.main.async {
                self.delegate?.itemAdded(item, index: self.items.count-1)
            }
        }
        
        if let minWeight = minimumWeight {
            if minWeight < item.weightValue {
                addItem()
            }
        } else {
            addItem()
        }
    }
}
