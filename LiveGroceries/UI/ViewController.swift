//
//  ViewController.swift
//  LiveGroceries
//
//  Created by Roie Shimon Cohen on 16/10/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITextFieldDelegate, GroceriesFeed {
    
    let groceriesManager = GroceriesManager.shared
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        groceriesManager.delegate = self
        groceriesManager.connectToWebSocket()
    }

    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        groceriesManager.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        let items = groceriesManager.items
        let item = items[items.count-1-indexPath.row]
        cell.nameLabel.text = item.name
        cell.weightLabel.text = item.weight
        cell.circleImageView.tintColor = Utilities.hexStringToUIColor(hex: item.bagColor)
        return cell
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        groceriesManager.minimumWeight = (textField.text as NSString?)?.floatValue
    }
    
    
    //MARK: - GroceriesFeed
    func itemAdded(_ item: Item, index: Int) {
        tableView.insertRows(at: [IndexPath(row: groceriesManager.items.count-1-index, section: 0)], with: .top)
    }
}

