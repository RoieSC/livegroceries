//
//  ItemCell.swift
//  LiveGroceries
//
//  Created by Roie Shimon Cohen on 16/10/2020.
//  Copyright © 2020 RoieSC. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {
    
    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.addBorder(width: 1.0, cornerRadius: 3.0)
    }
}
